<?php
/**
 *
 * Dictionary Generator 0.3
 *
 * While it generates the dictionary, it try to also
 * encrypt that passwords printing out to screen the
 * results. One line foreach password.
 *
 * You have to provide to it all possible characters 
 * inside the password by separating it with '-'.
 *
 * Usage: php ./insaneDGen.php a-b-c
 *
 * ToDo:
 *     - Change possible characters token ( actually: - )
 *     - Choose encrypt algorithms by argv
 *     - Enable more available encrypt algorithms
 *     - Put on MySQL database all data
 *     - Resume operations and distributing jobs system
 *
**/

$alpha = array( "a", "b", "c");/*, "d", "e", "f", "g", "h", "i", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "z", "w", "y", "j", "k", "x" );
$extra = array( "�", "�", "�", "�", "�", "�" );
$num   = array( "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" );
$chars = array( '!', '"', '�', '$', '%', '&', '/', '(', ')', '=', '\'', '?', '�', '^', '\\', '<', '>', ',', '.', '-', ';', ':', '-', '@', '#', ']', '[', '�', '�', '*', '+', '�', '|' );
$dictionary = array_merge( $alpha, array_merge( $extra, array_merge( $num, $chars ) ) );
*/
$dictionary = ( !empty( $_SERVER[argv][1] ) ) ? explode( '-', $_SERVER[argv][1] ) : $alpha;

$end_num     = count( $dictionary );

// Now we try 1 char passwords
foreach ( $dictionary as $d )
{
/*
    $esito = $db->query( 'INSERT INTO ipp_passwords '
                       . '( timestamp, password, md5, base64, crc ) '
                       . 'VALUES ( "' . time( ) . "\", \"$base_password_prefix$password_prefix$d\", '" . md5( $base_password_prefix . $password_prefix . $d ) . '\', "' . base64_encode( $base_password_prefix . $password_prefix . $d ) . '", "' . crc32( $base_password_prefix . $password_prefix . $d ) . '", "' . crypt( $base_password_prefix . $password_prefix . $d ) . '" )' );
*/
    print "$base_password_prefix$password_prefix$d:" . md5( $base_password_prefix . $password_prefix . $d ) . ":" . base64_encode( $password_prefix . $d ) . ":" . crc32( $base_password_prefix . $password_prefix . $d ) . ":" . crypt( $base_password_prefix . $password_prefix . $d ) . "\n";
}

// And now all the rest ;)
for ( $g = 0; $end_num >= $g; $g++ )
{
    for ( $c = 0; ( $end_num - 1) >= $c; $c++ )
    {
        if ( ! empty( $dictionary[$c] ) ) $password_prefix = $dictionary[$c];
        foreach ( $dictionary as $d )
        {
    /*
            $esito = $db->query( 'INSERT INTO ipp_passwords '
                               . '( timestamp, password, md5, base64, crc ) '
                               . 'VALUES ( "' . time( ) . "\", \"$base_password_prefix$password_prefix$d\", '" . md5( $base_password_prefix . $password_prefix . $d ) . '\', "' . base64_encode( $base_password_prefix . $password_prefix . $d ) . '", "' . crc32( $base_password_prefix . $password_prefix . $d ) . '", "' . crypt( $base_password_prefix . $password_prefix . $d ) . '" )' );
    */
            print "$base_password_prefix$password_prefix$d:" . md5( $base_password_prefix . $password_prefix . $d ) . ":" . base64_encode( $password_prefix . $d ) . ":" . crc32( $base_password_prefix . $password_prefix . $d ) . ":" . crypt( $base_password_prefix . $password_prefix . $d ) . "\n";
        }
    }
    if ( ! empty( $dictionary[$g] ) ) $base_password_prefix = $dictionary[$g];
    // sleep(3); // Keep it disabled just in case you got a powerful cpu
}

print "pwned! :)\n";
